-- phpMyAdmin SQL Dump
-- version 3.3.3
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 29 2011 г., 13:21
-- Версия сервера: 5.5.16
-- Версия PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `textildo_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id_ph` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_ph` char(16) DEFAULT NULL,
  `category` enum('shtory','toys') NOT NULL,
  PRIMARY KEY (`id_ph`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Дамп данных таблицы `photos`
--

INSERT INTO `photos` (`id_ph`, `link_ph`, `category`) VALUES
(1, 'photo01.jpg', 'shtory'),
(2, 'photo02.jpg', 'shtory'),
(3, 'photo03.jpg', 'shtory'),
(4, 'photo04.jpg', 'shtory'),
(5, 'za01.jpg', 'toys'),
(6, 'za02.jpg', 'toys'),
(7, 'za03.jpg', 'toys'),
(8, 'za04.jpg', 'toys'),
(9, 'photo05.jpg', 'shtory'),
(10, 'photo06.jpg', 'shtory'),
(11, 'photo07.jpg', 'shtory'),
(12, 'photo08.jpg', 'shtory'),
(13, 'photo09.jpg', 'shtory'),
(14, 'photo10.jpg', 'shtory'),
(15, 'photo11.jpg', 'shtory'),
(16, 'photo12.jpg', 'shtory'),
(17, 'photo13.jpg', 'shtory'),
(18, 'photo14.jpg', 'shtory'),
(19, 'photo15.jpg', 'shtory'),
(20, 'photo16.jpg', 'shtory'),
(21, 'photo17.jpg', 'shtory'),
(22, 'photo18.jpg', 'shtory'),
(23, 'photo19.jpg', 'shtory'),
(24, 'photo20.jpg', 'shtory'),
(25, 'photo21.jpg', 'shtory'),
(26, 'photo22.jpg', 'shtory'),
(27, 'photo23.jpg', 'shtory'),
(28, 'photo24.jpg', 'shtory'),
(29, 'za05.jpg', 'toys'),
(30, 'za06.jpg', 'toys'),
(31, 'za07.jpg', 'toys'),
(32, 'za08.jpg', 'toys'),
(33, 'za09.jpg', 'toys'),
(34, 'za10.jpg', 'toys'),
(35, 'za11.jpg', 'toys'),
(36, 'za12.jpg', 'toys'),
(37, 'za13.jpg', 'toys'),
(38, 'za14.jpg', 'toys'),
(39, 'za15.jpg', 'toys'),
(40, 'za16.jpg', 'toys'),
(41, 'za17.jpg', 'toys'),
(42, 'za18.jpg', 'toys'),
(43, 'za19.jpg', 'toys'),
(44, 'za20.jpg', 'toys'),
(45, 'za21.jpg', 'toys'),
(46, 'za22.jpg', 'toys'),
(47, 'za23.jpg', 'toys'),
(48, 'za24.jpg', 'toys'),
(49, 'za25.jpg', 'toys'),
(50, 'za26.jpg', 'toys'),
(51, 'za27.jpg', 'toys'),
(52, 'za28.jpg', 'toys'),
(53, 'za29.jpg', 'toys'),
(54, 'za30.jpg', 'toys'),
(55, 'za31.jpg', 'toys'),
(56, 'za32.jpg', 'toys'),
(57, 'za33.jpg', 'toys'),
(58, 'za34.jpg', 'toys'),
(59, 'za35.jpg', 'toys'),
(60, 'za36.jpg', 'toys'),
(61, 'za37.jpg', 'toys'),
(62, 'za38.jpg', 'toys'),
(63, 'za39.jpg', 'toys'),
(64, 'za40.jpg', 'toys'),
(65, 'za41.jpg', 'toys');