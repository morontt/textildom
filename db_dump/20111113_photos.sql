-- phpMyAdmin SQL Dump
-- version 3.4.3
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 13 2011 г., 21:45
-- Версия сервера: 5.5.13
-- Версия PHP: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `textildo_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id_ph` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link_ph` char(16) DEFAULT NULL,
  `category` enum('shtory','toys') NOT NULL,
  PRIMARY KEY (`id_ph`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `photos`
--

INSERT INTO `photos` (`id_ph`, `link_ph`, `category`) VALUES
(1, 'photo01.jpg', 'shtory'),
(2, 'photo02.jpg', 'shtory'),
(3, 'photo03.jpg', 'shtory'),
(4, 'photo04.jpg', 'shtory'),
(5, 'za01.jpg', 'toys'),
(6, 'za02.jpg', 'toys'),
(7, 'za03.jpg', 'toys'),
(8, 'za04.jpg', 'toys');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
