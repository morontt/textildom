<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $linkClass = array(
            'home'     => '',
            'shtory'   => '',
            'kovr'     => '',
            'toys'     => '',
            'contacts' => ''
        );
        $this->view->linkClass = $linkClass;
    }

    public function indexAction()
    {
        $this->view->headerphoto = true;

        $this->view->linkClass['home'] = 'class="current"';
    }

    public function kovrAction()
    {
        $this->view->linkClass['kovr'] = 'class="current"';
    }

    public function contactAction()
    {
        $this->view->headerphoto = true;

        $this->view->linkClass['contacts'] = 'class="current"';
    }

    public function galleryAction()
    {
        $id = $this->getRequest()->getParam('id');
        $gid = $this->getRequest()->getParam('gid');

        if ($id > 1) {
            $this->_redirect($this->view->url(array(), $gid, true), array('code' => 301));
        }

        $this->view->id = $id;
        $this->view->gid = $gid;

        $photos = new Application_Model_DbTable_Photos();

        $result = $photos->getByType($gid);

        if (!isset($result['error'])) {
            $this->view->result = $result;
        } else {
            $this->gotoError404();
        }

        $this->view->linkClass[$gid] = 'class="current"';
    }

    public function ajaxgalleryAction()
    {
        $id = $this->getRequest()->getParam('id');
        $gid = $this->getRequest()->getParam('gid');

        $this->_helper->layout->disableLayout();

        $this->view->id = $id;
        $this->view->gid = $gid;

        $photos = new Application_Model_DbTable_Photos();

        $result = $photos->getById($id, $gid);

        $this->view->result = $result;

    }

    public function previewAction()
    {
        $type = $this->getRequest()->getParam('type');
        $filename = $this->getRequest()->getParam('filename');
        $hight = $this->getRequest()->getParam('hight');

        $this->_helper->layout->disableLayout();

        $img = Application_Model_Img::getImage($filename, $type, $hight);

        $this->view->img = $img;
    }

    protected function gotoError404()
    {
        $this->getResponse()->setHttpResponseCode(404);
        $this->view->message = 'Страница не найдена';
        $this->view->error404 = true;

        return $this->_request->setControllerName('error')
                              ->setActionName('error');
    }
}









