<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initRouters()
    {
        $controller = Zend_Controller_Front::getInstance();
        $router = $controller->getRouter();
        
        $router->addRoute('home', new Zend_Controller_Router_Route('/',
                                                array('module'      => 'default',
                                                      'controller'  => 'index', 
                                                      'action'      => 'index')));
        
        $router->addRoute('kovr', new Zend_Controller_Router_Route('/kovriki',
                                                array('module'      => 'default',
                                                      'controller'  => 'index', 
                                                      'action'      => 'kovr')));
        
        $router->addRoute('contacts', new Zend_Controller_Router_Route('/contacts',
                                                array('module'      => 'default',
                                                      'controller'  => 'index', 
                                                      'action'      => 'contact')));
        
        $router->addRoute('shtory', new Zend_Controller_Router_Route('/shtory/:id',
                                                array('module'      => 'default',
                                                      'controller'  => 'index',
                                                      'action'      => 'gallery',
                                                      'gid'         => 'shtory',
                                                      'id'          => 1 )));
        
        $router->addRoute('toys', new Zend_Controller_Router_Route('/toys/:id',
                                                array('module'      => 'default',
                                                      'controller'  => 'index',
                                                      'action'      => 'gallery',
                                                      'gid'         => 'toys',
                                                      'id'          => 1 )));
        
        $router->addRoute('ajaxgallery', new Zend_Controller_Router_Route('/ajax/:gid/:id',
                                                array('module'      => 'default',
                                                      'controller'  => 'index',
                                                      'action'      => 'ajaxgallery',
                                                      'gid'         => 'shtory',
                                                      'id'          => 1 ),
                                                array('gid' => '(toys|shtory)')));
        
        $router->addRoute('login', new Zend_Controller_Router_Route('/login',
                                                array('module'      => 'default',
                                                      'controller'  => 'admin',
                                                      'action'      => 'login')));
        
        $router->addRoute('logout', new Zend_Controller_Router_Route('/logout',
                                                array('module'      => 'default',
                                                      'controller'  => 'admin',
                                                      'action'      => 'logout')));
        
        $router->addRoute('admin_index', new Zend_Controller_Router_Route('/admin',
                                                array('module'      => 'default',
                                                      'controller'  => 'admin',
                                                      'action'      => 'index')));
        
        $router->addRoute('admin_picture', new Zend_Controller_Router_Route('/admin_picture',
                                                array('module'      => 'default',
                                                      'controller'  => 'admin',
                                                      'action'      => 'picture')));
        
        $router->addRoute('admin_del', new Zend_Controller_Router_Route('/admin_del/:page',
                                                array('module'      => 'default',
                                                      'controller'  => 'admin',
                                                      'action'      => 'delete',
                                                      'page'        => 1,
                                                    )));
        
        $router->addRoute('delete_image', new Zend_Controller_Router_Route('/delete_image/:id',
                                                array('module'      => 'default',
                                                      'controller'  => 'admin',
                                                      'action'      => 'deleteimage',
                                                      'id'          => 0,
                                                    )));
        
        $router->addRoute('preview', new Zend_Controller_Router_Route('/thumbs/:type/:hight/:filename',
                                                array('module'      => 'default',
                                                      'controller'  => 'index',
                                                      'action'      => 'preview'),
                                                array('type' => '(toys|shtory)')));
        
        $controller->setRouter($router);
        
        }
}

