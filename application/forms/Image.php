<?php

class Application_Form_Image extends Zend_Form
{

    public function init()
    {
        $this->setMethod("post");
        $this->setName("teaser");
        
        $typePictureElement = new Zend_Form_Element_Select('type_picture');
        $typePictureElement->setLabel('Тип фотографии')
                           ->addMultiOptions(array(
                                'shtory' => 'Шторы',
                                'toys'   => 'Игрушки'));
        $this->addElement($typePictureElement);
        
        $picElement = new Zend_Form_Element_File('file_picture');
        $picElement->setRequired(false)
                   ->setDestination(realpath(dirname('.')) . '/images/tmp/')
                   ->setLabel('Файл (максимальный размер 2MB)')
                   ->addValidator('Extension', false, 'jpg,jpeg')
                   ->addValidator('Size', false, array('max' => '2MB'));
        $this->addElement($picElement);
        
        $submitButton = new Zend_Form_Element_Submit('submitButton');
        $submitButton->setLabel('Отправить');
        $this->addElement($submitButton);
    }


}

