<?php

class Application_Model_DbTable_Users extends Zend_Db_Table_Abstract
{
    protected $_name = 'users';
    
    public function loginUser($login, $pass)
    {
        $hashPassword = md5($pass . md5($pass));

        $auth = Zend_Auth::getInstance();
        $authAdapter = new Zend_Auth_Adapter_DbTable($this->getAdapter(), 'users');
        $authAdapter->setIdentityColumn('login')
                    ->setCredentialColumn('pass')
                    ->setIdentity($login)
                    ->setCredential($hashPassword);

        $result = $auth->authenticate($authAdapter);
        if ($result->isValid()) {
            $storage = new Zend_Auth_Storage_Session();
            $storage->write(
                $authAdapter->getResultRowObject(
                    array(
                        'user_id',
                        'login',
                    )
                )
            );
        }

        return $result->isValid();
    }
}
