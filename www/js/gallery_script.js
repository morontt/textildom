jQuery(document).ready(function() {
    jQuery('a.gall').click(function() {
        var link = '/ajax' + jQuery(this).attr('href');
        jQuery.ajax({
            url: link,
            dataType: 'json',
            type: 'POST',
            success: function(data) {
                jQuery('img#gimg').attr('src', data['img']);
                jQuery('a#prev1').attr('href', data['linkPrev']);
                jQuery('a#next1').attr('href', data['linkNext']);
                jQuery('a#next2').attr('href', data['linkNext']);
                jQuery('span#sch').text(data['imgId']);
            }
        });
        return false;
    });
});